Rails.application.routes.draw do
  resources :stocks
  devise_for :users
  get 'home/index'
  get 'home/about'
  root 'home#index'
  post "/" => 'home#index'
end
